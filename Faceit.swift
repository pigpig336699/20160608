//
//  Faceit.swift
//  test0608
//
//  Created by Dan on 2016/6/8.
//  Copyright © 2016年 Dan. All rights reserved.
//

import UIKit



class Faceit: UIView  {

    
    var timer = NSTimer();

  

    override func drawRect(rect: CGRect) {

        
        
        drawCircle(min(bounds.size.width,bounds.size.height) / 2, c: CGPoint(x: bounds.midX,y: bounds.midY), color: UIColor.yellowColor())
   
    

        drawCircle(min(bounds.size.width,bounds.size.height) / 2*0.02, c: CGPoint(x: bounds.midX*0.73,y: bounds.midY*0.7), color: UIColor.blackColor())
        drawCircle(min(bounds.size.width,bounds.size.height) / 2*0.02, c: CGPoint(x: bounds.midX*1.27,y: bounds.midY*0.7), color: UIColor.blackColor())
        
        
        let line = UIBezierPath()
        line.moveToPoint(CGPointMake(bounds.midX*1.27,bounds.midY*0.76))
        line.addLineToPoint(CGPointMake(bounds.midX*1.27, bounds.midY*0.9))
                UIColor.blueColor().set()
        line.stroke()

        
        
        drip(CGPointMake(bounds.midX*1.27,bounds.midY*0.76),
            EP: CGPointMake(bounds.midX*1.27, bounds.midY*0.9),
            CP: CGPointMake(bounds.midX*1.25, bounds.midY*0.8),
            CP2: CGPointMake(bounds.midX*1.09, bounds.midY*0.89),color: UIColor.blueColor())
 
        drip(CGPointMake(bounds.midX*1.27,bounds.midY*0.76),
            EP: CGPointMake(bounds.midX*1.27, bounds.midY*0.9),
            CP: CGPointMake(bounds.midX*1.29, bounds.midY*0.8),
            CP2: CGPointMake(bounds.midX*1.45, bounds.midY*0.89),color: UIColor.blueColor())
        
        
        
        
      
        
        

        
        
    }
   
    var i = 0
    func updata(){

        drip(CGPointMake(bounds.midX*1.27,bounds.midY*0.76),
            EP: CGPointMake(bounds.midX*1.27, bounds.midY*0.9),
            CP: CGPointMake(bounds.midX*1.29, bounds.midY*0.8),
            CP2: CGPointMake(bounds.midX*1.45, bounds.midY*0.89),color: UIColor.whiteColor())


    
    }
    
    
    func drawCircle (r:CGFloat,c:CGPoint,color:UIColor){
        
        let circle = UIBezierPath(arcCenter: c, radius: r, startAngle: 0.0, endAngle: CGFloat(M_PI*2), clockwise: false)
        
        circle.lineWidth=10.0
        
        color.set()
        circle.stroke()
        circle.fill()

    
    
    }
    
    func drawhelfCircle (r:CGFloat,c:CGPoint,color:UIColor,sa:CGFloat,ea:CGFloat){
        let circle = UIBezierPath(arcCenter: c, radius: r, startAngle: sa, endAngle: ea, clockwise: true)
        
        circle.lineWidth=5.0
        
        color.set()
        circle.stroke()
      
        
    
    
    }
    
    func drip(SP:CGPoint,EP:CGPoint,CP:CGPoint,CP2:CGPoint,color: UIColor){
        
        let startPoint = SP
        let endPoint = EP
        
        let controlPoint = CP
        let controlPoint2 = CP2
        
        let path = UIBezierPath()
       path.lineWidth = 1.9
        
        path.moveToPoint(startPoint)
        path.addCurveToPoint(endPoint, controlPoint1: controlPoint, controlPoint2: controlPoint2)
        color.set()
        color.setFill()
        path.fill()
        path.stroke()
        
        
     
     
        
        
    }
    

  
    

}
